<?php

namespace App\DataFixtures;
use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategorieFixtures extends Fixture
{ private array $categorie = ['naturalisme','pictorialisme', 'photo-secession', 'dadaisme','suréalisme', 'constructivisme'];
    public function load(ObjectManager $manager): void
    {
        foreach ($this->categorie as $category ){
            $cat = new Categorie();
            $cat->setName($category);
            $manager->persist($cat);

        }
        $manager->flush();
    }
}
