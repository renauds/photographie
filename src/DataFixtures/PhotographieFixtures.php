<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Technique;
use App\Entity\Photographie;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PhotographieFixtures extends Fixture implements DependentFixtureInterface
{private array $naturalisme = [['la cueuillette des nénuphars','Dans cette photographie, une femme se penche pour cueillir des nénuphars qui serviront d\'appâts au pêcheur. Son ombre et celles du rameur, des avirons et de la barque se mirent dans l\'eau immobile qui les entoure : quotidien fixé dans une image d\'harmonie entre les travailleurs et le paysage. « La cueillette des nénuphars » fut publiée dans « Life and Landscape on the Norfolk Broads », une collaboration entre Emerson et le peintre Thomas F. Goodall. Les deux font connaissance à l\'occasion d\'une croisière d\'agrément sur les Norfolk Broads, un réseau de lacs et de rivières très fréquenté par les touristes. Le recueil comprend quarante épreuves au platine, dont les douces gradations tonales rendent à merveille l\'atmosphère des marais.','01-01-1885','now',19.9,29.1,'01.jpg'],['ricking the reeds','orem ipsum dolor sit amet, eloquentiam liberavisse eum ne, vix ad viris percipitur honestatis. Ius unum delenit in. Fabulas assentior an nam, pri mazim legimus adolescens et, qui ea tation oporteat. Ut eam latine inciderint, usu modus omnesque ei. Et vis facilis detraxit.

Odio errem vim ex, ex alia essent consetetur pri, an blandit hendrerit sed. Ex eos utamur molestie. Nec cu tale tritani inciderint, vim ea simul tollit concludaturque, at nibh ubique ridens sea. Vim ex elit aeque, usu democritum dissentias dissentiunt at, his iriure alterum recteque in. Meis laudem repudiandae has ad, duo munere postea mentitum no.

Iisque luptatum his cu. Eos ut quem iusto zril. Tale cetero offendit sed ne, ut est fugit labore eloquentiam. Vix cu zril dictas necessitatibus, an mei ludus lobortis delicata, mea an habeo neglegentur concludaturque. Eu minim accumsan oporteat nec, per id atqui aperiri concludaturque. Vel ex apeirian salutatus disputationi, esse error partiendo eam te.','01-01-1886','now',23.2,28.4,'02.jpg'],
    ['march reeds','orem ipsum dolor sit amet, eloquentiam liberavisse eum ne, vix ad viris percipitur honestatis. Ius unum delenit in. Fabulas assentior an nam, pri mazim legimus adolescens et, qui ea tation oporteat. Ut eam latine inciderint, usu modus omnesque ei. Et vis facilis detraxit.

Odio errem vim ex, ex alia essent consetetur pri, an blandit hendrerit sed. Ex eos utamur molestie. Nec cu tale tritani inciderint, vim ea simul tollit concludaturque, at nibh ubique ridens sea. Vim ex elit aeque, usu democritum dissentias dissentiunt at, his iriure alterum recteque in. Meis laudem repudiandae has ad, duo munere postea mentitum no.

Iisque luptatum his cu. Eos ut quem iusto zril. Tale cetero offendit sed ne, ut est fugit labore eloquentiam. Vix cu zril dictas necessitatibus, an mei ludus lobortis delicata, mea an habeo neglegentur concludaturque. Eu minim accumsan oporteat nec, per id atqui aperiri concludaturque. Vel ex apeirian salutatus disputationi, esse error partiendo eam te.','01-01-1895','now',10.0,14.3,'03.jpg'],['great yarmouth exibition','orem ipsum dolor sit amet, eloquentiam liberavisse eum ne, vix ad viris percipitur honestatis. Ius unum delenit in. Fabulas assentior an nam, pri mazim legimus adolescens et, qui ea tation oporteat. Ut eam latine inciderint, usu modus omnesque ei. Et vis facilis detraxit.

Odio errem vim ex, ex alia essent consetetur pri, an blandit hendrerit sed. Ex eos utamur molestie. Nec cu tale tritani inciderint, vim ea simul tollit concludaturque, at nibh ubique ridens sea. Vim ex elit aeque, usu democritum dissentias dissentiunt at, his iriure alterum recteque in. Meis laudem repudiandae has ad, duo munere postea mentitum no.

Iisque luptatum his cu. Eos ut quem iusto zril. Tale cetero offendit sed ne, ut est fugit labore eloquentiam. Vix cu zril dictas necessitatibus, an mei ludus lobortis delicata, mea an habeo neglegentur concludaturque. Eu minim accumsan oporteat nec, per id atqui aperiri concludaturque. Vel ex apeirian salutatus disputationi, esse error partiendo eam te.','01-01-1896','now',20.0,28.0,'04.jpg'],['L\'ancien ordre et l\'ordre moderne','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum sapien nec nisi dictum condimentum. Sed maximus elit fermentum, mattis elit vitae, sollicitudin nibh. Vestibulum eget auctor enim, vel maximus velit. Maecenas purus turpis, aliquet in leo ac, molestie consectetur metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sit amet felis id viverra. Fusce venenatis in justo eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

Morbi dictum libero et dui scelerisque tristique. Proin tempor varius convallis. Curabitur porttitor interdum sem, sit amet maximus elit commodo in. Donec fringilla nisi libero, in fringilla lacus luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel porta orci, id placerat risus. Phasellus efficitur nunc at ipsum suscipit, in fringilla nunc iaculis. Sed semper volutpat elementum. Nunc ut augue semper purus molestie dapibus. Ut eu orci neque. Donec eget leo orci. Suspendisse suscipit, urna ut sollicitudin dapibus, lorem metus egestas diam, vel sagittis quam nisl et neque. In vel venenatis diam, eget sagittis sapien. Donec ultrices tellus sit amet leo condimentum condimentum. Etiam eget ex mi. Vestibulum in felis ac lectus scelerisque efficitur.

Donec tempus metus in mattis dapibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis molestie ligula leo, quis vulputate arcu varius eu. Sed hendrerit, tellus ac ultricies pulvinar, eros ligula dignissim erat, imperdiet vehicula risus nibh eget nisl. Etiam fringilla vestibulum consectetur. Sed mollis cursus vehicula. Phasellus scelerisque, dui eget sodales lobortis, elit ligula vehicula ante, at semper ipsum dolor eu nulla. Aenean eget congue enim, convallis pharetra ex. Suspendisse quis dui libero. Suspendisse malesuada erat quam, vitae volutpat odio fermentum nec.','01-01-1885','now',26.9,22.3,'05.jpg'],['De retour des marais','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum sapien nec nisi dictum condimentum. Sed maximus elit fermentum, mattis elit vitae, sollicitudin nibh. Vestibulum eget auctor enim, vel maximus velit. Maecenas purus turpis, aliquet in leo ac, molestie consectetur metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sit amet felis id viverra. Fusce venenatis in justo eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

Morbi dictum libero et dui scelerisque tristique. Proin tempor varius convallis. Curabitur porttitor interdum sem, sit amet maximus elit commodo in. Donec fringilla nisi libero, in fringilla lacus luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel porta orci, id placerat risus. Phasellus efficitur nunc at ipsum suscipit, in fringilla nunc iaculis. Sed semper volutpat elementum. Nunc ut augue semper purus molestie dapibus. Ut eu orci neque. Donec eget leo orci. Suspendisse suscipit, urna ut sollicitudin dapibus, lorem metus egestas diam, vel sagittis quam nisl et neque. In vel venenatis diam, eget sagittis sapien. Donec ultrices tellus sit amet leo condimentum condimentum. Etiam eget ex mi. Vestibulum in felis ac lectus scelerisque efficitur.

Donec tempus metus in mattis dapibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis molestie ligula leo, quis vulputate arcu varius eu. Sed hendrerit, tellus ac ultricies pulvinar, eros ligula dignissim erat, imperdiet vehicula risus nibh eget nisl. Etiam fringilla vestibulum consectetur. Sed mollis cursus vehicula. Phasellus scelerisque, dui eget sodales lobortis, elit ligula vehicula ante, at semper ipsum dolor eu nulla. Aenean eget congue enim, convallis pharetra ex. Suspendisse quis dui libero. Suspendisse malesuada erat quam, vitae volutpat odio fermentum nec.','01-01-1886','now',26.9,22.3,'06.jpg'],['A rushy shore','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum sapien nec nisi dictum condimentum. Sed maximus elit fermentum, mattis elit vitae, sollicitudin nibh. Vestibulum eget auctor enim, vel maximus velit. Maecenas purus turpis, aliquet in leo ac, molestie consectetur metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sit amet felis id viverra. Fusce venenatis in justo eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

Morbi dictum libero et dui scelerisque tristique. Proin tempor varius convallis. Curabitur porttitor interdum sem, sit amet maximus elit commodo in. Donec fringilla nisi libero, in fringilla lacus luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel porta orci, id placerat risus. Phasellus efficitur nunc at ipsum suscipit, in fringilla nunc iaculis. Sed semper volutpat elementum. Nunc ut augue semper purus molestie dapibus. Ut eu orci neque. Donec eget leo orci. Suspendisse suscipit, urna ut sollicitudin dapibus, lorem metus egestas diam, vel sagittis quam nisl et neque. In vel venenatis diam, eget sagittis sapien. Donec ultrices tellus sit amet leo condimentum condimentum. Etiam eget ex mi. Vestibulum in felis ac lectus scelerisque efficitur.

Donec tempus metus in mattis dapibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis molestie ligula leo, quis vulputate arcu varius eu. Sed hendrerit, tellus ac ultricies pulvinar, eros ligula dignissim erat, imperdiet vehicula risus nibh eget nisl. Etiam fringilla vestibulum consectetur. Sed mollis cursus vehicula. Phasellus scelerisque, dui eget sodales lobortis, elit ligula vehicula ante, at semper ipsum dolor eu nulla. Aenean eget congue enim, convallis pharetra ex. Suspendisse quis dui libero. Suspendisse malesuada erat quam, vitae volutpat odio fermentum nec.','01-01-1886','now',26.9,22.3,'07.jpg'],['Le ramassage du foin','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum sapien nec nisi dictum condimentum. Sed maximus elit fermentum, mattis elit vitae, sollicitudin nibh. Vestibulum eget auctor enim, vel maximus velit. Maecenas purus turpis, aliquet in leo ac, molestie consectetur metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus maximus sit amet felis id viverra. Fusce venenatis in justo eu venenatis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae;

Morbi dictum libero et dui scelerisque tristique. Proin tempor varius convallis. Curabitur porttitor interdum sem, sit amet maximus elit commodo in. Donec fringilla nisi libero, in fringilla lacus luctus ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vel porta orci, id placerat risus. Phasellus efficitur nunc at ipsum suscipit, in fringilla nunc iaculis. Sed semper volutpat elementum. Nunc ut augue semper purus molestie dapibus. Ut eu orci neque. Donec eget leo orci. Suspendisse suscipit, urna ut sollicitudin dapibus, lorem metus egestas diam, vel sagittis quam nisl et neque. In vel venenatis diam, eget sagittis sapien. Donec ultrices tellus sit amet leo condimentum condimentum. Etiam eget ex mi. Vestibulum in felis ac lectus scelerisque efficitur.

Donec tempus metus in mattis dapibus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis molestie ligula leo, quis vulputate arcu varius eu. Sed hendrerit, tellus ac ultricies pulvinar, eros ligula dignissim erat, imperdiet vehicula risus nibh eget nisl. Etiam fringilla vestibulum consectetur. Sed mollis cursus vehicula. Phasellus scelerisque, dui eget sodales lobortis, elit ligula vehicula ante, at semper ipsum dolor eu nulla. Aenean eget congue enim, convallis pharetra ex. Suspendisse quis dui libero. Suspendisse malesuada erat quam, vitae volutpat odio fermentum nec.','01-01-1886','now',26.9,22.3,'08.jpg']];
    private array $pictorialisme = [['Disparaissant','Lorem ipsum dolor sit amet. Et architecto eaque qui provident inventore et quae quibusdam ea suscipit possimus. Et alias sunt et saepe quibusdam a alias optio eos iure exercitationem.

Et rerum sint aut eveniet vero ut eaque temporibus ex nobis rerum sed similique sint ex iure tempore sit exercitationem aliquid. Aut aspernatur maiores et eaque molestiae ex iure molestias ut alias libero aut debitis nihil ea voluptates quam!

Quo ipsum sint sed beatae ullam eum magni aliquid sit pariatur consequatur et quae officia sed modi dolorum nam distinctio earum! Et expedita facere vel Quis perspiciatis sed facere omnis quo voluptas tempora ut quia necessitatibus?','01-01-1858','now',26.9,22.3,'09.jpg'],['Nature morte','Lorem ipsum dolor sit amet. Et architecto eaque qui provident inventore et quae quibusdam ea suscipit possimus. Et alias sunt et saepe quibusdam a alias optio eos iure exercitationem.

Et rerum sint aut eveniet vero ut eaque temporibus ex nobis rerum sed similique sint ex iure tempore sit exercitationem aliquid. Aut aspernatur maiores et eaque molestiae ex iure molestias ut alias libero aut debitis nihil ea voluptates quam!

Quo ipsum sint sed beatae ullam eum magni aliquid sit pariatur consequatur et quae officia sed modi dolorum nam distinctio earum! Et expedita facere vel Quis perspiciatis sed facere omnis quo voluptas tempora ut quia necessitatibus?','01-01-1908','now',20.1,15.8,'10.jpg'],['Claire de lune à New-york','Lorem ipsum dolor sit amet. Et architecto eaque qui provident inventore et quae quibusdam ea suscipit possimus. Et alias sunt et saepe quibusdam a alias optio eos iure exercitationem.

Et rerum sint aut eveniet vero ut eaque temporibus ex nobis rerum sed similique sint ex iure tempore sit exercitationem aliquid. Aut aspernatur maiores et eaque molestiae ex iure molestias ut alias libero aut debitis nihil ea voluptates quam!

Quo ipsum sint sed beatae ullam eum magni aliquid sit pariatur consequatur et quae officia sed modi dolorum nam distinctio earum! Et expedita facere vel Quis perspiciatis sed facere omnis quo voluptas tempora ut quia necessitatibus?','01-01-1904','now',23.2,18.5,'11.jpg'],['Mignon','Lorem ipsum dolor sit amet. Et architecto eaque qui provident inventore et quae quibusdam ea suscipit possimus. Et alias sunt et saepe quibusdam a alias optio eos iure exercitationem.

Et rerum sint aut eveniet vero ut eaque temporibus ex nobis rerum sed similique sint ex iure tempore sit exercitationem aliquid. Aut aspernatur maiores et eaque molestiae ex iure molestias ut alias libero aut debitis nihil ea voluptates quam!

Quo ipsum sint sed beatae ullam eum magni aliquid sit pariatur consequatur et quae officia sed modi dolorum nam distinctio earum! Et expedita facere vel Quis perspiciatis sed facere omnis quo voluptas tempora ut quia necessitatibus?','01-01-1900','now',21.2,16.3,'12.jpg']];
    private array $secession = [['la main de l\'homme','Lorem ipsum dolor sit amet. Ut recusandae blanditiis sit modi magnam nam tenetur quia eum illo dolorum et odit eveniet nam laborum aspernatur. Est voluptatem beatae et tempora obcaecati et quas odit non eligendi autem aut ducimus sunt!

Ab maxime animi hic consequuntur repudiandae est asperiores quos et minima eius hic optio omnis. Et nihil doloremque in dolores modi qui quisquam quis.

Sed ipsum internos et internos laborum id officia quidem ea esse esse. Non beatae beatae ab cumque saepe et aliquid galisum et consequatur eius a mollitia omnis et deserunt unde. A consequatur quibusdam et voluptatem voluptatibus in nemo excepturi ad debitis dolores At sint veritatis quo consequatur voluptas? Ut consequatur voluptatum et consequatur dignissimos qui quibusdam provident vel voluptate fuga eos quas voluptatibus quo voluptate illum aut dolores fuga!','01-01-1902','now',24.2,31.9,'13.jpg'],['le réparareur de filet','Lorem ipsum dolor sit amet. Ut recusandae blanditiis sit modi magnam nam tenetur quia eum illo dolorum et odit eveniet nam laborum aspernatur. Est voluptatem beatae et tempora obcaecati et quas odit non eligendi autem aut ducimus sunt!

Ab maxime animi hic consequuntur repudiandae est asperiores quos et minima eius hic optio omnis. Et nihil doloremque in dolores modi qui quisquam quis.

Sed ipsum internos et internos laborum id officia quidem ea esse esse. Non beatae beatae ab cumque saepe et aliquid galisum et consequatur eius a mollitia omnis et deserunt unde. A consequatur quibusdam et voluptatem voluptatibus in nemo excepturi ad debitis dolores At sint veritatis quo consequatur voluptas? Ut consequatur voluptatum et consequatur dignissimos qui quibusdam provident vel voluptate fuga eos quas voluptatibus quo voluptate illum aut dolores fuga!','01-01-1894','now',20.3,16.5,'14.jpg'],['Minuit au lac George','Lorem ipsum dolor sit amet. Ut recusandae blanditiis sit modi magnam nam tenetur quia eum illo dolorum et odit eveniet nam laborum aspernatur. Est voluptatem beatae et tempora obcaecati et quas odit non eligendi autem aut ducimus sunt!

Ab maxime animi hic consequuntur repudiandae est asperiores quos et minima eius hic optio omnis. Et nihil doloremque in dolores modi qui quisquam quis.

Sed ipsum internos et internos laborum id officia quidem ea esse esse. Non beatae beatae ab cumque saepe et aliquid galisum et consequatur eius a mollitia omnis et deserunt unde. A consequatur quibusdam et voluptatem voluptatibus in nemo excepturi ad debitis dolores At sint veritatis quo consequatur voluptas? Ut consequatur voluptatum et consequatur dignissimos qui quibusdam provident vel voluptate fuga eos quas voluptatibus quo voluptate illum aut dolores fuga!','01-01-1904','now',20.8,17.9,'15.jpg'],['Une nuit glaciale à NY','Lorem ipsum dolor sit amet. Ut recusandae blanditiis sit modi magnam nam tenetur quia eum illo dolorum et odit eveniet nam laborum aspernatur. Est voluptatem beatae et tempora obcaecati et quas odit non eligendi autem aut ducimus sunt!

Ab maxime animi hic consequuntur repudiandae est asperiores quos et minima eius hic optio omnis. Et nihil doloremque in dolores modi qui quisquam quis.

Sed ipsum internos et internos laborum id officia quidem ea esse esse. Non beatae beatae ab cumque saepe et aliquid galisum et consequatur eius a mollitia omnis et deserunt unde. A consequatur quibusdam et voluptatem voluptatibus in nemo excepturi ad debitis dolores At sint veritatis quo consequatur voluptas? Ut consequatur voluptatum et consequatur dignissimos qui quibusdam provident vel voluptate fuga eos quas voluptatibus quo voluptate illum aut dolores fuga!','01-01-1898','now',21.8,18.3,'16.jpg'],['Le terminal','Lorem ipsum dolor sit amet. Ut recusandae blanditiis sit modi magnam nam tenetur quia eum illo dolorum et odit eveniet nam laborum aspernatur. Est voluptatem beatae et tempora obcaecati et quas odit non eligendi autem aut ducimus sunt!

Ab maxime animi hic consequuntur repudiandae est asperiores quos et minima eius hic optio omnis. Et nihil doloremque in dolores modi qui quisquam quis.

Sed ipsum internos et internos laborum id officia quidem ea esse esse. Non beatae beatae ab cumque saepe et aliquid galisum et consequatur eius a mollitia omnis et deserunt unde. A consequatur quibusdam et voluptatem voluptatibus in nemo excepturi ad debitis dolores At sint veritatis quo consequatur voluptas? Ut consequatur voluptatum et consequatur dignissimos qui quibusdam provident vel voluptate fuga eos quas voluptatibus quo voluptate illum aut dolores fuga!','01-01-1893','now',17.3,15.9,'17.jpg']];

    private array $dadaisme =[['fenetre sur Berlin','Prise depuis la fenêtre de son appartement berlinois en 1931, cette photo redécouverte à la fin des années 1970 dit beaucoup du regard critique que portait Raoul Hausmann sur la modernité. Contrairement à certains artistes de son temps ayant travaillé sur la ville en proposant une vision quasi démiurgique de l’homme, lui montre ici, au contraire, son obsession du petit, de l’anodin, du solitaire.','01-01-1931','now',30.5,40,'18.jpg'],['Dunes, mer Baltique','Derrière la simplicité formelle de ce cliché de dune se cache l’une des préoccupations de l’artiste : l’écologie. Preuve du temps qui passe, du flux et de l’instabilité permanente, ces dunes disparaissent en effet peu à peu du fait de l’activité de l’homme et du dérèglement climatique déjà en cours. Il développera ainsi tout un travail sur la nature et les plantes, avec la conviction que l’homme n’est qu’une espèce vivante parmi d’autres, dans un environnement qu’il ne dominera jamais vraiment.','01-01-1927','now',17,23,'19.jpg'],['Maison paysanne',' Basé sur un principe de modularité, ce type de construction vernaculaire, qui remonte sûrement au XVIe siècle, renvoie à bien des égards à l’architecture fonctionnaliste.','01-01-1934','now',30,39,'20.jpg'],['Au café, il n\'y a que des hommes','Alors que ce territoire est considéré par beaucoup comme arriéré, l’artiste se sent au contraire très proche de ses habitants, qu’il ne cessera de montrer et de décrire comme des anonymes dignes et forts, très heureux à l’écart du monde moderne.','01-01-1933','now',17.2,24.5,'21.jpg']];
    private array $surealisme = [['Portrait de l\'espace','Suspendisse facilisis turpis accumsan sagittis fringilla. Nunc sed magna ligula. Praesent mollis euismod eros, ac accumsan massa aliquet vitae. Integer aliquet lobortis velit eget consequat. Fusce sed quam sit amet odio ultrices accumsan. Sed laoreet purus purus, sed eleifend dolor venenatis sit amet. Aliquam efficitur aliquet posuere. Maecenas pharetra erat vel mi auctor ultrices. Duis sodales mi eu dolor bibendum ultrices. Vivamus pulvinar ac quam a semper. Aenean dolor mauris, rutrum vitae fringilla sodales, luctus eget nibh. Duis aliquet consectetur elit vitae tincidunt. Mauris ultrices augue quis erat tincidunt, tristique elementum nulla mattis. In hac habitasse platea dictumst. Nam fermentum condimentum enim sed molestie.

Cras eu pretium nisi. Vivamus lacinia nisl nec ipsum malesuada volutpat. Sed scelerisque magna sit amet nulla mattis, ac ornare lacus tempus. Phasellus sed mauris ac velit elementum porttitor non vitae turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum luctus non purus eget placerat. Pellentesque at mattis ligula. Aenean lorem felis, posuere at arcu sed, consequat congue enim. Donec ullamcorper sem nibh, vel sagittis tellus pulvinar et.','01-01-1937','now',20.3,21.8,'22.jpg'],['La chanteuse de l\'opéra','Suspendisse facilisis turpis accumsan sagittis fringilla. Nunc sed magna ligula. Praesent mollis euismod eros, ac accumsan massa aliquet vitae. Integer aliquet lobortis velit eget consequat. Fusce sed quam sit amet odio ultrices accumsan. Sed laoreet purus purus, sed eleifend dolor venenatis sit amet. Aliquam efficitur aliquet posuere. Maecenas pharetra erat vel mi auctor ultrices. Duis sodales mi eu dolor bibendum ultrices. Vivamus pulvinar ac quam a semper. Aenean dolor mauris, rutrum vitae fringilla sodales, luctus eget nibh. Duis aliquet consectetur elit vitae tincidunt. Mauris ultrices augue quis erat tincidunt, tristique elementum nulla mattis. In hac habitasse platea dictumst. Nam fermentum condimentum enim sed molestie.

Cras eu pretium nisi. Vivamus lacinia nisl nec ipsum malesuada volutpat. Sed scelerisque magna sit amet nulla mattis, ac ornare lacus tempus. Phasellus sed mauris ac velit elementum porttitor non vitae turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum luctus non purus eget placerat. Pellentesque at mattis ligula. Aenean lorem felis, posuere at arcu sed, consequat congue enim. Donec ullamcorper sem nibh, vel sagittis tellus pulvinar et.','01-01-1945','now',31.8,31.3,'23.jpg'],['Paris 1944','Suspendisse facilisis turpis accumsan sagittis fringilla. Nunc sed magna ligula. Praesent mollis euismod eros, ac accumsan massa aliquet vitae. Integer aliquet lobortis velit eget consequat. Fusce sed quam sit amet odio ultrices accumsan. Sed laoreet purus purus, sed eleifend dolor venenatis sit amet. Aliquam efficitur aliquet posuere. Maecenas pharetra erat vel mi auctor ultrices. Duis sodales mi eu dolor bibendum ultrices. Vivamus pulvinar ac quam a semper. Aenean dolor mauris, rutrum vitae fringilla sodales, luctus eget nibh. Duis aliquet consectetur elit vitae tincidunt. Mauris ultrices augue quis erat tincidunt, tristique elementum nulla mattis. In hac habitasse platea dictumst. Nam fermentum condimentum enim sed molestie.

Cras eu pretium nisi. Vivamus lacinia nisl nec ipsum malesuada volutpat. Sed scelerisque magna sit amet nulla mattis, ac ornare lacus tempus. Phasellus sed mauris ac velit elementum porttitor non vitae turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum luctus non purus eget placerat. Pellentesque at mattis ligula. Aenean lorem felis, posuere at arcu sed, consequat congue enim. Donec ullamcorper sem nibh, vel sagittis tellus pulvinar et.','01-01-1944','now',27.1,18.9,'24.jpg'],['Le nu de Bent Forward','Suspendisse facilisis turpis accumsan sagittis fringilla. Nunc sed magna ligula. Praesent mollis euismod eros, ac accumsan massa aliquet vitae. Integer aliquet lobortis velit eget consequat. Fusce sed quam sit amet odio ultrices accumsan. Sed laoreet purus purus, sed eleifend dolor venenatis sit amet. Aliquam efficitur aliquet posuere. Maecenas pharetra erat vel mi auctor ultrices. Duis sodales mi eu dolor bibendum ultrices. Vivamus pulvinar ac quam a semper. Aenean dolor mauris, rutrum vitae fringilla sodales, luctus eget nibh. Duis aliquet consectetur elit vitae tincidunt. Mauris ultrices augue quis erat tincidunt, tristique elementum nulla mattis. In hac habitasse platea dictumst. Nam fermentum condimentum enim sed molestie.

Cras eu pretium nisi. Vivamus lacinia nisl nec ipsum malesuada volutpat. Sed scelerisque magna sit amet nulla mattis, ac ornare lacus tempus. Phasellus sed mauris ac velit elementum porttitor non vitae turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum luctus non purus eget placerat. Pellentesque at mattis ligula. Aenean lorem felis, posuere at arcu sed, consequat congue enim. Donec ullamcorper sem nibh, vel sagittis tellus pulvinar et.','01-01-1930','now',21.1,23.9,'25.jpg']];
    private array $constructivisme =[['L\'escalier','
Vivamus tincidunt rhoncus maximus. Pellentesque blandit aliquam neque bibendum pharetra. Etiam sagittis eu magna molestie vulputate. Donec at viverra ligula. Quisque at facilisis justo, at laoreet dui. Cras nec metus nunc. Aliquam erat volutpat. Aenean nisi lectus, posuere id luctus ut, mollis nec velit. Nulla tempus volutpat arcu. Aenean ornare faucibus nisi.

Nulla euismod, orci sed feugiat fringilla, diam eros fringilla libero, eu hendrerit urna velit dapibus tortor. Suspendisse ut tincidunt justo. Morbi id lorem libero. Phasellus vitae leo nunc. Pellentesque ex diam, tempor nec orci eleifend, pharetra sodales nunc. Nam porttitor in tellus in consequat. Vestibulum lacus orci, maximus eget eros ut, suscipit tristique lacus. Fusce sed dui eleifend, eleifend dui sed, elementum quam.','01-01-1930','now',25.0,16.2,'26.jpg'],['Construction du canal Baltique-mer blanche','Mauris ultricies mauris nulla. Aenean aliquam erat non ante posuere suscipit. Cras varius ipsum laoreet pellentesque malesuada. Proin nec facilisis est, vitae mattis quam. In sit amet neque sed ipsum pulvinar consequat eu a ipsum. Proin malesuada tellus justo, ac egestas felis feugiat sed. Nulla et sagittis urna. Curabitur imperdiet porta scelerisque. Nulla a magna id orci fermentum vulputate sit amet vitae lectus. Quisque a massa ante. Sed vel lorem ipsum. Duis feugiat tempor aliquam. Etiam auctor tempus malesuada.

Donec id eleifend lectus. Proin eget felis ac lacus facilisis commodo. Sed arcu erat, vehicula nec lectus non, mattis fringilla massa. Donec rhoncus, tellus et tempus blandit, ipsum ex ullamcorper sapien, eget viverra felis dolor eu nulla. Sed venenatis tincidunt erat, tempor pellentesque enim posuere eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis vitae erat neque. Nunc mattis lobortis metus et vulputate. Aliquam et malesuada metus. Nullam posuere laoreet convallis. Sed lorem elit, varius sit amet egestas a, lobortis in massa. Quisque tempus risus id odio dapibus, eget finibus arcu sodales.','01-01-1933','now',33.9,23.0,'27.jpg'],['La jeune fille au Leica','Vivamus interdum arcu augue, vel efficitur leo sodales non. Sed viverra non erat gravida dignissim. Morbi rhoncus sodales hendrerit. Cras et rhoncus risus. Fusce et convallis lacus. Nulla magna est, finibus luctus iaculis et, tempor dignissim lacus. Duis in dui a ligula tristique fringilla vitae eu felis.

Donec eget neque dapibus, hendrerit justo et, fermentum ante. Nam arcu nunc, congue nec pretium vel, viverra non elit. Integer non purus quis magna facilisis egestas luctus quis mi. Ut arcu leo, malesuada at nulla sit amet, aliquam tempus nulla. Curabitur ante nisl, varius vitae vehicula et, venenatis quis magna. Maecenas lacus nisi, aliquet eu dignissim imperdiet, efficitur non lorem. Nulla euismod eget justo eget pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.','01-01-1934','now',19.3,27.7,'28.jpg'],['Plongeon','Vivamus volutpat ut purus vel lacinia. Cras eu eleifend nunc. Morbi vulputate libero leo, sed rutrum ante blandit eu. Sed et blandit quam. Praesent non vehicula urna. Pellentesque vitae felis sit amet nunc commodo porttitor non at massa. Nam commodo ligula at tincidunt viverra. Fusce quis lacus nisi. Ut accumsan ligula vitae mauris pulvinar luctus. Nulla vitae mattis lorem, eget tempus augue.

Proin vel nisl sed tellus auctor semper. Praesent laoreet nulla non odio tempus, eu faucibus ex tristique. Sed eu ex molestie, interdum odio laoreet, porttitor nibh. Sed dictum urna posuere, laoreet erat et, sollicitudin ex. Vivamus egestas magna in dapibus porttitor. Sed ut leo elit. Phasellus consectetur odio vel urna sagittis, congue pretium metus faucibus. Cras vitae lectus purus. Mauris sodales est sit amet bibendum scelerisque. Morbi eu aliquam metus. Proin posuere nunc in dui tempor lacinia. Aenean elementum ut sem sit amet aliquet. Sed iaculis purus ut elementum mattis. Etiam a mollis sem, id tincidunt purus. Praesent vestibulum molestie tortor, et commodo tellus finibus at. Ut condimentum magna ante, et scelerisque eros aliquet nec.','01-01-1934','now',50.8,67.7,'29.jpg']];
    public function load(ObjectManager $manager): void
    {$categorie = $manager->getRepository(Categorie::class)->findAll();
        $technique = $manager->getRepository(Technique::class)->findAll();
        $slugify = new Slugify();
        foreach ($this->naturalisme as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[0]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10, 100));
            $manager->persist($photo);


        }
        foreach ($this->pictorialisme as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[1]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10,100));
            $manager->persist($photo);


        }
        foreach ($this->secession as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[2]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10,100));
            $manager->persist($photo);


        }
        foreach ($this->dadaisme as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[3]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10,100));
            $manager->persist($photo);


        }
        foreach ($this->surealisme as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[4]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10,100));
            $manager->persist($photo);


        }
        foreach ($this->constructivisme as $photography ){

            $photo = new Photographie();
            $photo->setTechnique($technique[0]);
            $photo->setCategorie($categorie[5]);
            $photo->setTitre($photography[0]);
            $photo->setDescription($photography[1]);
            $photo->setCreatedAt(new \DateTimeImmutable($photography[2]));
            $photo->setRegisterAt(new  \DateTimeImmutable($photography[3]));
            $photo->setHauteur($photography[4]);
            $photo->setLargeur($photography[5]);
            $photo->setSlug($slugify->slugify($photography[0]));
            $photo->setImageName($photography[6]);
            $photo->setPrice(rand(10,100));
            $manager->persist($photo);


        }


        $manager->flush();


    }

    public function getDependencies ()
    {
        return [
            CategorieFixtures::class, TechniqueFixtures::class
        ];
    }
}
