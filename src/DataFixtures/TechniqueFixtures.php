<?php

namespace App\DataFixtures;
use App\Entity\Technique;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TechniqueFixtures extends Fixture
{
    private array $categorie = ['monochrome','couleur'];
    public function load(ObjectManager $manager): void
    {
        foreach ($this->categorie as $category ){
            $cat = new Technique();
            $cat->setName($category);
            $manager->persist($cat);

        }
        $manager->flush();
    }
}
