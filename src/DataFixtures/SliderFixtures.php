<?php

namespace App\DataFixtures;
use App\Entity\Slider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SliderFixtures extends Fixture
{private array $slider =['slide01.jpg', 'slide02.jpg', 'slide03.jpg', 'slide04.jpg','slide05.jpg','slide06.jpg', 'slide07.jpg','slide08.jpg','slide09.jpg'];
    public function load(ObjectManager $manager): void
    {
       foreach ($this->slider as $slide){
           $s = new Slider();
           $s->setImageName($slide);
           $s->setIsdisabled(0);
           $s->setCreatedAt(new \DateTimeImmutable());
           $manager->persist($s);
       }

        $manager->flush();
    }
}
