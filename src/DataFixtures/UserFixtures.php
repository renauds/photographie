<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
class UserFixtures extends Fixture
{private array $roles = [['ROLE_USER']];
    private object $hasher;
    private array $genders =['male', 'female'];

    public function __construct(UserPasswordHasherInterface $hasher){

        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $slug = new Slugify();//nettoyer les prefixe du mail
        for($i = 1; $i<= 50; $i++){
            foreach ($this->roles as $role ){

                $role = $this->roles[0];
            }
            $user = new User();
            $gender = $faker->randomElement($this->genders);
            $user->setFirstName($faker->firstName($gender))
                ->setLastName($faker->lastName)
                ->setEmail($slug->slugify($user->getFirstName()) . '.' .$slug->slugify($user->getLastName()) . '@'
                    .$faker->freeEmailDomain());

            $gender = $gender == 'male' ? 'm' : 'f';
            $user->setImageName('0'. ($i + 10) .$gender . '.jpg')
                ->setPassword($this->hasher->hashPassword($user,'password'))
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable())
                ->setIsDisabled($faker->boolean(10))
                ->setRoles($role);

            $manager->persist($user);


        }
        //admin Jhon Doe
        $user = new User();

        $user->setFirstName('Jhon')
            ->setLastName('Doe')
            ->setEmail('jhon.doe@hotmail.com')


            ->setImageName('062m.jpg')
            ->setPassword($this->hasher->hashPassword($user,'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        $manager->flush();

        // Super Admin renaud S

        $user = new User();

        $user->setFirstName('Renaud')
            ->setLastName('Saintenoy')
            ->setEmail('saintrenaud@hotmail.com')


            ->setImageName('016m.jpg')
            ->setPassword($this->hasher->hashPassword($user,'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);
        $manager->flush();

        // Super Admin renaud S

        $user = new User();

        $user->setFirstName('Pat')
            ->setLastName('Mar')
            ->setEmail('patmar@gmail.com')


            ->setImageName('015m.jpg')
            ->setPassword($this->hasher->hashPassword($user,'password'))
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setIsDisabled(false)
            ->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);
        $manager->flush();

    }

}
