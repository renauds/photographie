<?php

namespace App\Controller;

use App\Entity\Like;
use App\Form\EditPasswordUserType;
use App\Form\EditProfileType;
use App\Repository\LikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @param LikeRepository $likeRepository
     * @return Response
     */
    #[Route('/user', name: 'profile')]
    public function index(LikeRepository $likeRepository): Response
    {$user = $this->getUser();
        $like = $likeRepository->findBy(['user' => $user]);
        return $this->render('user/profile.html.twig', [

            'like'=>$like

        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse|Response
     */
    #[Route('/user/edit-profile/',name:'app_edit_profile')]
    public function editProfile(Request $request, EntityManagerInterface $manager)
    {
        $user = $this->getUser();//idem que app.de twig ici le getUser est la medodemagique du usermanager
        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $user->setUpdatedAt(new \DateTimeImmutable());
            $manager->flush();
            return $this->redirectToRoute('profile');
        }
        return $this->renderForm('user/editprofile.html.twig', [
            'form' => $form
        ]);
    }

    /**
     * @param Like $like
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/user/dellike/{id}', name: 'dellike')]
    public function dellike(Like $like,  EntityManagerInterface $manager): Response {

        $manager->remove($like);
        $manager->flush();

        return $this->redirectToRoute('profile');
    }


     #[Route("/user/change-password", name:"change_password")]

    public function changePassword(Request $request, UserPasswordHasherInterface $userPasswordHasher,EntityManagerInterface $entityManager )
    {
        // Récupération de l'utilisateur connecté
        $user = $this->getUser();

        // Création du formulaire de changement de mot de passe
        $form = $this->createForm(EditPasswordUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encode le nouveau mot de passe

            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )

            );
            $user->setUpdatedAt(new \DateTimeImmutable());



            // Sauvegarde de l'utilisateur

            $entityManager->persist($user);
            $entityManager->flush();

            // Redirection vers la page de profil
            return $this->redirectToRoute('profile');
        }

        return $this->render('user/changepassword.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
