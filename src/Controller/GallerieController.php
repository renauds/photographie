<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\Photographie;
use App\Form\CommentType;
use App\Repository\CategorieRepository;
use App\Repository\CommentRepository;
use App\Repository\LikeRepository;
use App\Repository\PhotographieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GallerieController extends AbstractController
{
    /**
     * @param PhotographieRepository $photographieRepository
     * @param CategorieRepository $categorieRepository
     * @param LikeRepository $likeRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/gallerie', name: 'gallerie')]
    public function index(PhotographieRepository $photographieRepository,CategorieRepository $categorieRepository,
                          LikeRepository $likeRepository,
                          PaginatorInterface $paginator, Request $request ): Response
    { $tableau = $photographieRepository->findBy([], ['categorie' => 'ASC']);
        $categorie = $categorieRepository->findAll();
        $like = $likeRepository->findAll();
        $pagination = $paginator->paginate(
            $tableau,
            $request->query->getInt('page', 1),12);
        return $this->render('gallerie/gallerie.html.twig', [
            'home' => $tableau,
            'home' => $pagination,
            'categories'=>$categorie,
            'like'=>$like
        ]);
    }

    /**
     * @param Photographie $photographie
     * @param CommentRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('photographie/{slug}', name: 'onephotography')]
    public function photographie(Photographie $photographie,
                           CommentRepository      $repository,
                           Request                $request,
                           EntityManagerInterface $manager): Response

        // Form Comment
    {$comment = $repository->findBy(['photographie' => $photographie], ['createdAt' => 'DESC']);
        $commentaire = new Comment();
        $user = $this->getUser();

        $form = $this->createForm(CommentType::class, $commentaire);//si je met createFormBuilder => il faut mettre
        // creatView en bas
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {

            $commentaire->setCreatedAt(new \DateTimeImmutable("now"));

            $commentaire->setPhotographie($photographie);
            $commentaire->setUser($user);
            $manager->persist($commentaire);
            $manager->flush();
            $this->addFlash('notice', 'votre commentaire a bien ete ajouté');

        }elseif ($form->isSubmitted()){
            $this->addFlash('alerte', "l'envoi du formulaire a echoué");
        }
        return $this->render('gallerie/onephotography.html.twig', [
            'photographie' =>$photographie,
            'comments' => $comment,
            'form' => $form->createView(),//si je met CreateView bas besoin de createView
        ]);
    }

    /**
     * @param Photographie $photographie
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('like/{id}', name: 'likephotography')]
    public function votes(Photographie $photographie,

                                 EntityManagerInterface $manager): Response
    {

        $like = new Like();

        $user = $this->getUser();

        $like->setUser($user);
        $like->setPhotographie($photographie);
        $like->setCreatedAt(new \DateTimeImmutable("now"));
        $like->setVote(1);
        $manager->persist($like);
        $manager->flush();
        return $this->redirectToRoute('gallerie');
    }

}
