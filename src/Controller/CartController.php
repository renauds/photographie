<?php

namespace App\Controller;
use App\Repository\PhotographieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @param SessionInterface $session
     * @param PhotographieRepository $photographieRepository
     * @return Response
     */
    #[Route('/panier', name: 'app_panier')]
    public function index(SessionInterface $session, PhotographieRepository $photographieRepository): Response
    {
        $panier = $session->get('panier', []);
        $panierData = [];
        foreach ($panier as $id => $quantity) {
            $panierData[] = [
                'product' => $photographieRepository->find($id),
                'quantity' =>$quantity,
            ];
        }

        $total = 0;
        foreach ($panierData as $item){
            $totalItem = $item['product']->getPrice() * $item['quantity'];
            $total += $totalItem;
        }

        return $this->render('panier/panier.html.twig', [
          'items'=>$panierData,
            'total'=>$total,
        ]);
    }

    /**
     * @param $id
     * @param SessionInterface $session
     * @return RedirectResponse
     */
    #[Route('/panier/add/{id}', name:'panier_add')]
    public function add($id, SessionInterface $session) {

        $panier = $session->get('panier', []);
        if(!empty($panier[$id])){
            $panier[$id]++;
        }else{
            $panier[$id] = 1 ;
        }

        $session->set('panier', $panier);
        return $this->redirectToRoute('app_panier');
    }

    /**
     * @param $id
     * @param SessionInterface $session
     * @return RedirectResponse
     */
    #[Route('/panier/min/{id}', name:'panier_min')]
    public function min($id, SessionInterface $session) {

        $panier = $session->get('panier', []);
        if(!empty($panier[$id])){
            $panier[$id]--;
        }

        if($panier[$id] < 1){

            unset($panier[$id]);
            $session->set('panier', $panier);
            return $this->redirectToRoute('app_panier');

        }

        $session->set('panier', $panier);
        return $this->redirectToRoute('app_panier');
    }


    /**
     * @param $id
     * @param SessionInterface $session
     * @return RedirectResponse
     */
    #[Route('/panier/remove/{id}', name:'panier_remove')]
    public function remove($id, SessionInterface $session){
        $panier = $session->get('panier',[]);
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        $session->set('panier', $panier);
        return $this->redirectToRoute('app_panier');
    }
}
