<?php

namespace App\Controller\Admin;
use App\Entity\Categorie;
use App\Entity\Slider;
use App\Form\SliderType;
use App\Repository\SliderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSliderController extends AbstractController
{
    /**
     * @param SliderRepository $sliderRepository
     * @return Response
     */
    #[Route('superadmin/slider', name: 'app_admin_slider')]
    public function index(SliderRepository $sliderRepository): Response
    {$slider = $sliderRepository->findBy(['Isdisabled'=>0],);

        return $this->render('/admin/partials/slider.html.twig', [
            'slider' => $slider,
        ]);
    }

    /**
     * @param SliderRepository $sliderRepository
     * @return Response
     */
    #[Route('superadmin/slider/view', name: 'app_admin_slider_view')]
    public function view(SliderRepository $sliderRepository): Response
    {$slider = $sliderRepository->findAll();

        return $this->render('/admin/partials/sliderall.html.twig', [
            'slider' => $slider,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse|Response
     */
    #[Route('/superadmin/slider/new', name: 'app_admin_slider_new')]
    public function new ( Request $request, EntityManagerInterface $manager )
    {
        $slider = new Slider();
        $form = $this->createForm(SliderType::class, $slider);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            $number = $manager->getRepository(Slider::class)->findAll();
            $count = count($number);
           if($count > 9){
               $this->addFlash('danger', 'vous avez trop de slider');
               return $this->redirectToRoute('app_admin_slider_view');
           }
            $slider->setCreatedAt(new \DateTimeImmutable("now"));
            $slider->setIsdisabled(0);
            $manager->persist($slider);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_slider_view');
        }
        return $this->renderForm('admin/partials/newslider.html.twig', [
            'form' => $form
        ]);
    }


    /**
     * @param Slider $slider
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/superadmin/slider/masque/{id}', name: 'app_admin_slider_masque')]
    public function masque (Slider $slider, EntityManagerInterface $manager ): Response
    {
        $slider->setIsdisabled(!$slider->isIsdisabled());
        $manager->flush();
        return $this->redirectToRoute('app_admin_slider_view');
    }

    /**
     * @param Slider $slider
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    #[Route('/superadmin/slider/delete/{id}', name: 'app_admin_slider_delete')]
    public function delete ( Slider $slider, EntityManagerInterface $manager )
    {
        $manager->remove($slider);
        $manager->flush();
        $this->addFlash('notice', 'votre action c\'est bien deroulée');
        return $this->redirectToRoute('app_admin_slider_view');
    }
}
