<?php

namespace App\Controller\Admin;
use App\Entity\Commande;
use App\Repository\CommandeRepository;
use App\Repository\PhotographieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CommandeController extends AbstractController
{
    /**
     * @param CommandeRepository $commandeRepository
     * @return Response
     */
    #[Route('admin/commande/all', name: 'app_admin_allcommande')]
    public function index(CommandeRepository $commandeRepository): Response
    {$commande = $commandeRepository->findAll();

        return $this->render('/admin/commande/allcommande.html.twig', [
            'commande' => $commande,
        ]);
    }


    /**
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    #[Route('enregistrecommande', name: 'app_enregistre_commande')]
    public function enregistreCommande(SessionInterface $session,PhotographieRepository $photographieRepository,
                                       EntityManagerInterface $manager)
    {
        {$panier = $session->get('panier', []);
            if($panier!=[]){
                $panierData = [];
                foreach ($panier as $id => $quantity) {
                    $panierData[] = [
                        'product' => $photographieRepository->find($id),
                        'quantity' =>$quantity,
                    ];
                }

                $total = 0;
                foreach ($panierData as $item){
                    $totalItem = $item['product']->getPrice() * $item['quantity'];
                    $total += $totalItem;
                }
            $user = $this->getUser();

            $date = date('dmyhis');
            $nameFichier = $user->getUserIdentifier().$date;
            // Configure Dompdf according to your needs
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');

            // Instantiate Dompdf with our options
            $dompdf = new Dompdf($pdfOptions);

            // Retrieve the HTML generated in our twig file
            $html = $this->renderView('admin/commande/enregistrecommande.html.twig', [
                'items'=>$panierData,
                'total'=>$total,
            ]);

            // Load HTML to Dompdf
            $dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
            $dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $dompdf->render();

            // Store PDF Binary Data
            $output = $dompdf->output();

            // In this case, we want to write the file in the public directory
            //$publicDirectory = $this->get('kernel')->getProjectDir() . '/public/upload/commande';
            // e.g /var/www/project/public/mypdf.pdf
            $pdfFilepath =  $this->getParameter('kernel.project_dir').'/public/upload/commande/'.$nameFichier.'.pdf';

            // Write file to the desired path
            file_put_contents($pdfFilepath, $output);
                $commande = new Commande();
                $commande->setUser($user);
                $commande->setCreatedAt(new \DateTimeImmutable());
                $commande->setFileName($nameFichier.'.pdf');
            $manager->persist($commande);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            // Send some text response
            return $this->redirectToRoute('app_panier');
        }else {  return $this->redirectToRoute('app_panier');}}
    }

}
