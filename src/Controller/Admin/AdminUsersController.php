<?php

namespace App\Controller\Admin;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminUsersController extends AbstractController
{
    /**
     * @param UserRepository $userRepository
     * @return Response
     */
    #[Route('/superadmin/users', name: 'app_admin_users')]
    public function adminuser(UserRepository $userRepository): Response
    {
        $users = $userRepository->findBy(
            [],
            ['createdAt' =>'DESC']
        );
        return $this->render('admin/users/users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @param User $user
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/superadmin/roleuser/{id}', name: 'app_admin_role_user')]
    public function roleUser( User $user, Request $request, EntityManagerInterface $manager): Response

    {$re = explode(" ",$request->get('role'));
        var_dump($re);
        if ($re == $user->getRoles()){
            $this->addFlash('danger',' le role de l\'utilisateur '. $user->getFirstName(). ' ' .$user->getLastName().
                ' est le meme.');
        }else{

            $user->setRoles($re);
            $manager->flush();
            $this->addFlash('success',' le role de l\'utilisateur '. $user->getFirstName(). ' ' .$user->getLastName(). ' a bien été modifié.');
        }
        return $this->redirectToRoute('app_admin_users');
    }
}
