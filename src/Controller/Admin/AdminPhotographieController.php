<?php

namespace App\Controller\Admin;
use App\Entity\Categorie;
use App\Entity\Photographie;
use App\Entity\Technique;
use App\Form\CategorieType;
use App\Form\PhotographieType;
use App\Form\TechniqueType;
use App\Repository\CommentRepository;
use App\Repository\LikeRepository;
use App\Repository\PhotographieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class AdminPhotographieController extends AbstractController
{
    /**
     * @param PhotographieRepository $photographieRepository
     * @return Response
     */
    #[Route('/superadmin/photographie', name: 'app_admin_photographie')]
    public function admin(PhotographieRepository $photographieRepository): Response
    { $photographies = $photographieRepository->findBy(

        [],['categorie' => 'ASC']);
        return $this->render('admin/photographie/photographies.html.twig', [
            'admin' => $photographies,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse|Response
     */
    #[Route('/superadmin/new', name: 'newphotographie')]
    public function new ( Request $request, EntityManagerInterface $manager )
    {
        $photographie = new Photographie();
        $form = $this->createForm(PhotographieType::class, $photographie);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            if(empty($photographie->getImageFile()))  $photographie->setImageName('default.jpg');
            $photographie->createSlug();
            $photographie->setRegisterAt(new \DateTimeImmutable("now"));
            $manager->persist($photographie);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_photographie');
        }



        return $this->renderForm('admin/photographie/newphotographie.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @param Photographie $photographie
     * @param CommentRepository $repository
     * @param LikeRepository $likeRepository
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    #[Route('/superadmin/delete/{id}', name: 'delete')]
    public function delete ( Photographie $photographie,  CommentRepository $repository, LikeRepository $likeRepository,
                             EntityManagerInterface
    $manager )
    {
        $comment = $repository->findBy(['photographie' => $photographie], ['createdAt' => 'DESC']);
        $like = $likeRepository->findBy(['photographie' => $photographie]);

        $commentcount= count($comment);
        $votecount = count($like);
        if ($votecount <1 AND $commentcount <1){
            $manager->remove($photographie);

            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_photographie');
        }else  $this->addFlash('danger', 'Vous ne pouvez pas effacer cette photographie, il existe des commentaires ou  des 
        likes'); return
        $this->redirectToRoute('app_admin_photographie'); }

    /**
     * @param Photographie $photographie
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/superadmin/edit/{id}', name: 'edit')]
    public function edit ( Photographie $photographie, EntityManagerInterface $manager, Request $request ): Response
    {
        $form = $this->createForm(PhotographieType::class, $photographie);
        $form->handleRequest($request);
        if ( $form->isSubmitted() && $form->isValid() ) {
            $photographie->createSlug();
            $manager->persist($photographie);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_photographie');
        }
        return $this->renderForm('/admin/photographie/editphotographie.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse|Response
     */
    #[Route('/superadmin/categorie', name: 'categorie')]
    public function categorie ( Request $request, EntityManagerInterface $manager )
    {
        $post = new Categorie();
        $form = $this->createForm(CategorieType::class, $post);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            $manager->persist($post);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_photographie');
        }
        return $this->renderForm('/admin/photographie/newcategorie.html.twig', [
            'form' => $form,

        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse|Response
     */
    #[Route('/superadmin/technique', name: 'technique')]
    public function technique ( Request $request, EntityManagerInterface $manager )
    {
        $post = new Technique();
        $form = $this->createForm(TechniqueType::class, $post);
        $form->handleRequest($request);//recupere les donees du formulaire
        if ( $form->isSubmitted() && $form->isValid() ) {
            $manager->persist($post);
            $manager->flush();
            $this->addFlash('success', 'votre action c\'est bien deroulée');
            return $this->redirectToRoute('app_admin_photographie');
        }
        return $this->renderForm('/admin/photographie/newtechnique.html.twig', [
            'form' => $form,

        ]);
    }
}
