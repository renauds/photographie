<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => 'Titre du commentaire',
                'attr' => [
                    'placeholder' => 'Titre du commentaire',]
            ])

            ->add('comment', TextType::class, [
                'label' => 'Votre commentaire',
                'attr' => [
                    'placeholder' => 'ici votre commentaire',]
            ])

            ->add('rating', IntegerType::class, [
                'label' => 'Votre cote',
                'attr' => [
                    'placeholder' => 'nombre entre 1 et 5',
                    'min' => 1,
                    'max' =>5,
                ]
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer'
            ]);


    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
