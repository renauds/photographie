<?php

namespace App\Form;
use App\Entity\Categorie;
use App\Entity\Technique;
use App\Entity\Photographie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
class PhotographieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class,[
                'label' => 'Titre de la photographie'
            ])
            ->add('description', TextareaType::class,[
                'label' => 'description de la photographie'
            ])
            ->add('createdAt', DateType::class,[
                'label' => 'Date de la création',
                'widget' => 'single_text',
                'html5'=>'false',
                'input'=>'datetime_immutable',
            ])
            ->add('hauteur', NumberType::class,[
                'label'=>'hauteur',
            ])
            ->add('largeur', NumberType::class,[
                'label'=>'largeur'
            ])


            ->add('technique', EntityType::class,[
                'class'=> Technique::class,
                'choice_label'=>'name'
            ])
            ->add('categorie', EntityType::class,[
                'class'=>Categorie::class,
                'choice_label'=>'name'
            ])
            ->add('price', MoneyType::class, [
                'label' => 'prix de la photographie',
                'grouping' => true,
                'required' => false,
                'scale' => 1,
                'empty_data' => '0.0',
            ] )
            ->add('imageFile',VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => false,
                'image_uri' => true,
            ]);



    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Photographie::class,
        ]);
    }
}
