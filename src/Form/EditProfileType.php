<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName',TextType::class, [
                'label' => 'Votre prénom',
                'attr' => [
                    'placeholder' => 'Janne',
                ]
            ])
            ->add('lastName',TextType::class, [
                'label' => 'Votre nom',
                'attr' => [
                    'placeholder' => 'Doe',
                ]
            ])
            ->add('email',EmailType::class, [
                'label' => 'Votre Email',
                'attr' => [
                    'placeholder' => 'janne.doe@exemple.com'
                ]
            ])
            ->add('imageFile',VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'delete_label' => '...',
                'download_label' => '...',
                'download_uri' => false,
                'image_uri' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
