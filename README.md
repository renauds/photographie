# photographie
Photographie est un petit site conçu dans le cadre du cours site dynamique 
de la deuxième année de la section webdeveloper de l'école EASCF-Namur-Cadet.

Le but de ce projet est d'approfondir la matière vue et d'améliorer ses 
connaissances pour le framework symfony.

Pour développer ce site, je suis parti d'une situation fictive d'une petite 
galerie d'exposition de photographies ancienne, voulant mettre en valeur ses 
plus belles photos et désirant en vendre des copies fac-similés.

# Ce qui est demandé:
- Une partie publique:
        Sur la partie publique, le sera accessible que la page d'acceuil, 
  une page About, Team et la gallerie d'image. De la gallerie, un lien sera 
  accessible afin de pouvoir lire la description de la photographie. Les liens 
  pour les likes, les commentaires et  le panier d'achat seront désactivés, si l'utilisateur n'est pas connecté. 
  Sera disponible aussi un lien d'enregistrement et de connection.

- La partie utilisateur: 
        Lorsque l'utilisateur est connecté, il aura accès a son profi, qu'il 
  pourra modifier. la modofication de son mot de passe sera aussi 
  accesssible. Il aura la permission de pouvoir liker une fois 
  chaque photographie présente sur la galerie et émettre un commentaire pour 
  chaque photographie. Il aurra accès au panier de commande. Chaque 
  photographie pour être commandée plusieures fois étant donné que ce seront 
  des copies.
- La partie Administrateur:
        Le role administrateur ne gerera que les commandes. Dans le cadre de 
  ce projet, ce sera la permission d'un accès à l'entié commande et de 
  pouvoir telecharger le pdf générer lors de la validation du panier.

- La partie Super Administrateur:
        Le role Super Admin n'aura pas accès aux commandes. Il aura en 
  gestion les photographies, les techniques et les catégories. Au niveau de 
  la gestion des photographies, il pourra enregistrer une nouvelles 
  photographie, éditer une photographies existante, ajouter une nouvelle 
  technique ou catégorie. 
        Il aura aussi en gestion les images du slider. Il pourra en masquer, 
  en ajouter ou e supprimer. 
        Il pourra aussi modofier les roles des utilisateurs enregistrer.

# Librairies utilisées

- Bootstrap pour le theme du site en général et la mise en page
- Jquerry pour le tableau affichage de la partie administration
- Filterz pour trier les cadres de la galerie de photographie
- un petit code javascript a été creer pour l'animation de la page 
  d'accueuil et le fadIn.

# Bundle utilisés dans le cadre de ce projet

- KpnPaginator pour la gestion de la pagination de la page de la gallerie
- liipImagine pour le redimensionnement des images: par exemple pour les 
  images des cadres de la galerie de photographie, des images slider,...
- recapcha(Victor): pour sécuriser le formulaire d'enregistrement
- Vichuploader: pour la gestion d'upload des images
- Dompdf: pour générer un pdf à partir d'une page HTML

Des bundle spécifique à symfony pour gerer les fixture et la gestion de la 
base de donnée (doctrine) ont aussi été installés

# Les roles disponibles

- pour acceder au site avec le role ADMIN: jhon.doe@hotmail.com
- pour acceder au site avec le role SUPER ADMIN: patmar@gmail.com
- le password est : password


